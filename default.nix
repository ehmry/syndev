{
  pkgs ? import <nixpkgs> { },
}:
with pkgs;
let
  buildNimSbom = pkgs.callPackage ./build-nim-sbom.nix { };
in
buildNimSbom (finalAttrs: {
  src = if lib.inNixShell then null else lib.cleanSource ./.;
}) ./sbom.json
