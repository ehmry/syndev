# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

# *Do not misinterpret any of this as documentation for handling Linux uevents.*

import
  std/[nativesockets, options, os, parseutils, posix, sets],
  pkg/sys/[handles, ioqueue, sockets],
  pkg/preserves,
  pkg/syndicate,
  pkg/syndicate/protocols/[gatekeeper, sturdy],
  ./schema/uevents

var
  AF_NETLINK {.importc, header: "sys/socket.h", nodecl.}: uint16
  NETLINK_KOBJECT_UEVENT {.importc, header: "linux/netlink.h", nodecl.}: cint

type
  Sockaddr_nl {.importc: "struct sockaddr_nl", header: "linux/netlink.h".} = object
    nl_family: uint16
    nl_pad: uint16
    nl_pid: uint32
    nl_groups: uint32

proc initSockaddr(family = AF_NETLINK; pid, groups: uint32): Sockaddr_nl =
  Sockaddr_nl(nl_family: family, nl_pid: pid, nl_groups: groups)

proc saddr(sa_nl: var Sockaddr_nl): ptr Sockaddr =
  cast[ptr Sockaddr](addr sa_nl)

proc openUeventSocket: SocketHandle =
  result = createNativeSocket(
      cint AF_NETLINK,
      cint posix.SOCK_DGRAM,
      NETLINK_KOBJECT_UEVENT,
    )
  var sa = initSockaddr(pid = uint32 getPid(), groups = 1)
  if bindAddr(result, saddr sa, SockLen sizeof(sa)) != 0:
    close(result)
    raiseOSError(osLastError(), "failed to bind Netlink socket")
  if sa.nl_family != AF_NETLINK:
    close(result)
    raise newException(IOError, "Netlink not supported")

type Socket = AsyncConn[sockets.Protocol.Unix]

proc openUeventSocketAsync: Socket =
  let fd = openUeventSocket()
  var flags = fcntl(fd.cint, F_GETFL)
  if flags < 0:
    raiseOSError(osLastError())
  flags = flags or O_NONBLOCK
  if fcntl(fd.cint, F_SETFL, flags) < 0:
    raiseOSError(osLastError())
  fd.SocketFD.initHandle.newAsyncSocket.Socket

type
  MessageRelay = ref object
    facet: Facet
    dataspace, attenuation: Cap
    sock: Socket

proc sendMessage(relay: MessageRelay; turn: Turn; buf: string; n: int) =
  var
    msg = initRecord("uevent", toPreserves"", toPreserves"", toSymbol"", initDictionary(), 0.toPreserves)
    key, val: string
    i = 0
  while i < n:
    inc i, skipWhile(buf, {'\0'}, i)
    inc i, parseUntil(buf, key, {'=', '@'}, i)
    if i < n:
      let sep = buf[i]
      inc i, parseUntil(buf, val, '\0', i+1)+1
      case sep
      of '@': discard
      of '=':
        if key == "SUBSYSTEM":
          msg.record[0].string = val
        elif key == "DEVPATH":
          msg.record[1].string = val
        elif key == "ACTION":
          msg.record[2].symbol = Symbol val
        elif key == "SEQNUM":
          msg.record[4] = parsePreserves(val)
        else:
          # TODO: check if val can be an integer
          var num: BiggestInt
          if parseBiggestInt(val, num) == val.len:
             add(msg.record[3].dict, (key.toSymbol, num.toPreserve,))
          else:
            add(msg.record[3].dict, (key.toSymbol, val.toPreserve,))
        val.setLen(0)
      else:
        stderr.writeLine "uevent parser failure"
        return
  if msg.record[3].dict.len > 0:
    cannonicalize(msg.record[3])
    message(turn, relay.dataspace, msg)
    msg.record[3].dict.setLen(0)

proc loop(relay: MessageRelay; ) {.asyncio.} =
  let buf = new string
  buf[].setLen(16 shl 10)
  while true:
    let n = relay.sock.read(buf)
    if n < 1: stopActor(relay.facet)
    else:
      proc act(turn: Turn) =
        relay.sendMessage(turn, buf[], n)
      relay.facet.run(act)

proc observeOnly: Caveat =
  """<reject <not <rec Observe [<_> <_>]>>>""".parsePreserves.preservesTo(Caveat).get

proc newMessageRelay(turn: Turn): MessageRelay =
  new result
  result.sock = openUeventSocketAsync()
  result.facet = turn.facet
  result.dataspace = turn.newDataspace()
  result.attenuation = result.dataspace.attenuate observeOnly()
  discard trampoline:
    whelp result.loop()
      # Start reading from the socket.

proc spawnUdevDumpActor*(turn: Turn; gatekeeper: Cap): Actor {.discardable.} =
  during(turn, gatekeeper, Resolve?:{ 0: UventDumpStep.matchType }) do:
    # At least one subscriber asserted.
    let relay = turn.newMessageRelay()
    during(turn, gatekeeper, Resolve?:{ 0: UventDumpStep.matchType, 1: grab() }) do (observer: Cap):
      publish(turn, observer, ResolvedAccepted(responderSession: relay.attenuation))
  do:
    relay.sock.close()
      # All subscribers retracted.

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; gatekeeper: Cap):
      spawnUdevDumpActor(turn, gatekeeper)
