
import
  preserves, std/tables

type
  UventDumpStep* {.preservesRecord: "uevent-dump".} = object
  
  Uevent* {.preservesRecord: "uvent".} = object
    `subsystem`*: string
    `devpath`*: string
    `action`*: string
    `attrs`*: Table[Symbol, string]
    `seqnum`*: BiggestInt

proc `$`*(x: UventDumpStep | Uevent): string =
  `$`(toPreserves(x))

proc encode*(x: UventDumpStep | Uevent): seq[byte] =
  encode(toPreserves(x))
